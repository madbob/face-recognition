<?php

namespace MadBob\FaceRecognition;

class Image
{
    private $image = null;

    public function __construct($parent, $path, $data)
    {
        $this->parent = $parent;
        $this->path = $path;
        $this->data = $data;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function hasFaces()
    {
        return empty($this->data) == false;
    }

    /*
        Returns an array of objects, each rappresenting a detected face
    */
    public function bboxes()
    {
        return $this->data;
    }

    private function prepareImage()
    {
        if (is_null($this->image)) {
            $this->image = $this->parent->initImage($this);
        }
    }

    public function coverDownFace($width, $height)
    {
        $this->prepareImage();

        /*
            If no faces are present, the image is just cropped and resized to
            the required size
        */
        if ($this->hasFaces() == false) {
            return $this->image->coverDown($width, $height);
        }

        /*
            Here I retrieve the full bounding box, which center will be then
            used as reference for the final crop
        */
        $bounding = $this->data[0];

        for ($i = 1; $i < count($this->data); $i++) {
            $iter = $this->data[$i];

            if ($bounding->x1 > $iter->x1) {
                $bounding->x1 = $iter->x1;
            }

            if ($bounding->x2 < $iter->x2) {
                $bounding->x2 = $iter->x2;
            }

            if ($bounding->y1 > $iter->y1) {
                $bounding->y1 = $iter->y1;
            }

            if ($bounding->y2 < $iter->y2) {
                $bounding->y2 = $iter->y2;
            }
        }

        $original_width = $this->image->width();
        $original_height = $this->image->height();

        $temp_height = $original_height;
        $temp_width = ($temp_height / $height) * $width;

        if ($temp_width > $original_width) {
            $temp_width = $original_width;
            $temp_height = ($temp_width / $width) * $height;
        }

        $x = max(0, ((($bounding->x2 - $bounding->x1) / 2) + $bounding->x1) - ($temp_width / 2));
        if ($x + $temp_width > $original_width) {
            $x = $original_width - $temp_width;
        }

        $y = max(0, ((($bounding->y2 - $bounding->y1) / 2) + $bounding->y1) - ($temp_height / 2));
        if ($y + $temp_height > $original_height) {
            $y = $original_height - $temp_height;
        }

        return $this->image->crop($temp_width, $temp_height, $x, $y)->resize($width, $height);
    }

    public function __call($name, $arguments)
    {
        $this->prepareImage();
        return $this->image->$name($arguments);
    }
}
