<?php

namespace MadBob\FaceRecognition;

use Intervention\Image\ImageManager;

class FaceRecognition
{
    private $params;
    private $imageManager;

    public function __construct($params = [])
    {
        $defaults = [
            'exec_path' => '',
            'model' => null,
            'cpus' => -1,
            'graphic_driver' => 'gd',
        ];

        $this->params = array_merge($defaults, $params);
    }

    private function buildCommand($path)
    {
        $exec_path = $this->params['exec_path'];
        if (empty($exec_path) == false && str_ends_with($exec_path, '/') == false) {
            $exec_path .= '/';
        }

        $command = 'face_detection';

        $params = [
            sprintf('--cpus %s', $this->params['cpus']),
        ];

        if ($this->params['model']) {
            $params[] = sprintf('--model %s', $this->params['model']);
        }

        $params = join(' ', $params);

        return sprintf('%s%s %s %s', $exec_path, $command, $params, $path);
    }

    private function mergeData($paths, $results)
    {
        $extracted = [];

        /*
            An Image is generated for each involved file, having faces or not.
            This is to simplify handling of the output
        */
        foreach($paths as $path) {
            $name = basename($path);

            $extracted[$name] = (object) [
                'path' => $path,
                'bboxes' => [],
            ];
        }

        $results = explode("\n", $results);
        foreach($results as $res) {
            $row = explode(',', $res);
            $name = basename($row[0]);

            if (isset($extracted[$name]) == false) {
                continue;
            }

            $extracted[$name]->bboxes[] = (object) [
                'x1' => $row[4],
                'x2' => $row[2],
                'y1' => $row[1],
                'y2' => $row[3],
            ];
        }

        $ret = [];

        foreach($extracted as $ext) {
            $img = new Image($this, $ext->path, $ext->bboxes);
            $ret[] = $img;
        }

        return $ret;
    }

    public function loadImage($path)
    {
        $command = $this->buildCommand($path);
        $result = shell_exec($command);
        $ret = $this->mergeData([$path], $result);
        return array_shift($ret);
    }

    public function loadFolder($path)
    {
        $paths = array_diff(scandir($path), ['.', '..']);
        $paths = array_map(fn($p) => rtrim($path, '/') . '/' . $p, $paths);
        $command = $this->buildCommand($path);
        $result = shell_exec($command);
        return $this->mergeData($paths, $result);
    }

    private function initImageManager()
    {
        if (is_null($this->imageManager)) {
            switch($this->params['graphic_driver']) {
                case 'gd':
                    $manager_driver = \Intervention\Image\Drivers\Gd\Driver::class;
                    break;
                case 'imagick':
                    $manager_driver = \Intervention\Image\Drivers\Imagick\Driver::class;
                    break;
                default:
                    break;
            }

            $this->imageManager = new ImageManager($manager_driver);
        }
    }

    public function initImage($item)
    {
        $this->initImageManager();
        return $this->imageManager->read($item->getPath());
    }
}
