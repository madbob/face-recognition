
# Face Recognition

This is a simple PHP wrapper for the popular [face-recognition](https://github.com/ageitgey/face_recognition) Python package.

On the contrary of other similar packages, this executes the CLI `face_detection` command and reads the output. This simplifies the deployment phase, as the `face-recognition` package can be installed in a Python isolated virtual environment (e.g. with [`pipx`](https://pipx.pypa.io/stable/), as recommended by Debian) which prevents the Python import of the library but still permits the execution of the application.

Plus, this library is integrated with [InterventionImage](https://image.intervention.io/) to provide convenient manipulation of the detected images.

This project has been initially implemented to better display photos of people retrieved from [Wikipedia Commons](https://commons.wikimedia.org/wiki/Main_Page) and published on [TrendingOnWiki](https://trendingon.wiki/).

## Install

```
# Or your preferred way to install the Python application
pipx install face-recognition

composer require madbob/face-recognition
```

## Example

```
require "vendor/autoload.php";

use MadBob\FaceRecognition\FaceRecognition;

$master = new FaceRecognition();
$image = $master->loadImage('test1.jpg');
$faces = $image->bboxes();
$image->coverDownFace(1920, 1080)->toJpeg()->save('test2.jpg');
```

## Documentation

First of all, you have to init an instance of `MadBob\FaceRecognition\FaceRecognition`, which constructor accepts an associative array of parameters.

The accepted parameters are:

| name | usage | default |
|--|--|--|
| exec_path | The path where to find the `face_detection` executable. By default it is empty, assuming it is already in $PATH | |
| model | The `face-recognition` model you prefer to use. Read the package's documentation for details | |
| cpus | Number of CPUs to use while reading the images. -1 means "use all in system" | -1 |
| graphic_driver | The driver to be used to handle images with InterventionImage. May be `gd` or `imagick` | `gd` |

```
$master = new FaceRecognition([
    'exec_path' => '/usr/local/bin',
    'model' => 'hog',
    'cpus' => 2,
    'graphic_driver' => 'imagick',
]);
```

On this instance, you can call two different function: `loadImage()` or `loadFolder()`. The first one takes the path of a single image, and returns a single `MadBob\FaceRecognition\Image` object, while the second parses all images in the given folder and returns an array of the same object type.

```
$image = $master->loadImage('/path/to/test1.jpg');
$images = $master->loadFolder('/path/to/many/photos/');
```

A `MadBob\FaceRecognition\Image` is mostly a a wrapper around an instance of `Intervention\Image\Image`: on this object you can call all the utility functions provided by InterventionImage (resize, crop, save...), plus a few custom ones.

- `getPath()` returns the path of the original image;
- `hasFaces()` returns `true` or `false` depending if the image contains at least a detected face;
- `bboxes()` returns an array of objects, one for each detected face, with attributes `x1`, `x2`, `y1` and `y2` holding his coordinates. If no faces have been detected, it returns an empty array;
- `coverDownFace($width, $height)` returns a `Intervention\Image\Image` instance already cropped and resized to keep the faces found in the image at the center; this can be chained with other InterventioImage functions to save the manipulated image or apply further transformations. If no face has been detected, the image is cropped and resized just at the center (using `coverDown()` function in InterventionImage).

```
$images = $master->loadFolder('/path/to/many/photos/');
foreach($images as $image) {
    echo $image->getPath() . ' : ' . count($image->bboxes() . "\n";
}
```

## License

This code is free software, licensed under the MIT License. See the LICENSE.txt file for more details.

Copyright (C) 2024 Roberto Guido info@madbob.org
